import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthLibService {
  private userNameSource = new BehaviorSubject<string>('');
  public userName$ = this.userNameSource.asObservable();

  private isAuthenticatedSource = new BehaviorSubject<boolean>(false);
  public isAuthenticated$ = this.isAuthenticatedSource.asObservable();

  private authTokenSource = new BehaviorSubject<string>('');
  public authToken$ = this.authTokenSource.asObservable();

  constructor() {
    // Lấy thông tin từ sessionStorage khi service được tạo
    this.loadFromSessionStorage();
  }

  private loadFromSessionStorage() {
    const userName = sessionStorage.getItem('userName');
    const isAuthenticated = sessionStorage.getItem('isAuthenticated') === 'true';
    const authToken = sessionStorage.getItem('authToken');

    if (userName && authToken) {
      this.userNameSource.next(userName);
      this.isAuthenticatedSource.next(isAuthenticated);
      this.authTokenSource.next(authToken);
    }
  }

  private saveToSessionStorage() {
    sessionStorage.setItem('userName', this.userNameSource.value);
    sessionStorage.setItem('isAuthenticated', this.isAuthenticatedSource.value.toString());
    sessionStorage.setItem('authToken', this.authTokenSource.value);
  }

  public get user(): string {
    return this.userNameSource.value;
  }

  public get isAuthenticated(): boolean {
    return this.isAuthenticatedSource.value;
  }

  public get authToken(): string {
    return this.authTokenSource.value;
  }

  public saveInfoUser(userName: string, authToken: string): void {
    // Assume successful saving of user information
    this.userNameSource.next(userName);
    this.authTokenSource.next(authToken);
    this.isAuthenticatedSource.next(true);
    // You may choose to generate a new token or get it from the server
    const token = 'example_token';
    this.authTokenSource.next(token);

    // Lưu thông tin vào sessionStorage
    this.saveToSessionStorage();
  }

  public deleteInfoUser(): void {
    // Clear user information
    this.userNameSource.next('');
    this.isAuthenticatedSource.next(false);
    this.authTokenSource.next('');

    // Xóa thông tin khỏi sessionStorage
    sessionStorage.removeItem('userName');
    sessionStorage.removeItem('isAuthenticated');
    sessionStorage.removeItem('authToken');
  }
}
