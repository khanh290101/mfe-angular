// login.component.ts
import { Component } from '@angular/core';
import { Router } from '@angular/router'; // Import Router từ Angular
import { AuthLibService } from 'auth-lib';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  username: string = '';
  password: string = '';

  // Inject Router vào constructor
  constructor(private router: Router,
    private authLibService: AuthLibService,
    private notificationService: NotificationService) {}

  login() {
    const hardcodedUsername = 'admin';
    const hardcodedPassword = '123';

    if (this.username === hardcodedUsername && this.password === hardcodedPassword) {
      this.notificationService.showMessage('Đăng nhập thành công!');
      this.authLibService.saveInfoUser(this.username, "example-token")
      this.router.navigate(['/screen1']);
    } else {
      this.notificationService.showMessage('Đăng nhập thất bại. Vui lòng kiểm tra lại thông tin đăng nhập.');
    }
  }
}
