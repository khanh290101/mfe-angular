
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isLoggedIn: boolean = false;
  private authToken: string = '';

  // Thông tin đăng nhập cứng
  private hardcodedUsername: string = 'admin';
  private hardcodedPassword: string = '123';

  constructor() {}

  login(username: string, password: string): boolean {
    // Kiểm tra thông tin đăng nhập
    if (username === this.hardcodedUsername && password === this.hardcodedPassword) {
      // Lưu thông tin đăng nhập và token vào Session Storage
      sessionStorage.setItem('isLoggedIn', 'true');
      this.isLoggedIn = true;

      // Tạo một token ở đây (có thể sử dụng thư viện như jwt.io)
      this.authToken = 'example_token';

      sessionStorage.setItem('authToken', this.authToken);

      return true;
    } else {
      return false;
    }
  }

  logout(): void {
    // Xóa thông tin đăng nhập và token từ Session Storage
    sessionStorage.removeItem('isLoggedIn');
    sessionStorage.removeItem('authToken');

    this.isLoggedIn = false;
    this.authToken = '';
  }

  getIsLoggedIn(): boolean {
    // Kiểm tra xem người dùng đã đăng nhập hay chưa
    return this.isLoggedIn || sessionStorage.getItem('isLoggedIn') === 'true';
  }

  getAuthToken(): string {
    // Lấy token từ Session Storage
    return this.authToken || sessionStorage.getItem('authToken') || '';
  }
}
