import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private _message = new BehaviorSubject<string>('');
  public message = this._message.asObservable();

  constructor() { }

  showMessage(message: string) {
    this._message.next(message);
    // Tự động ẩn thông báo sau 3 giây
    setTimeout(() => this._message.next(''), 3000);
  }
}
