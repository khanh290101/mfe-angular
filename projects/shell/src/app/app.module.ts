import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ShellHomeComponent } from './shell-home/shell-home.component';
import { LoadFragmentsComponent } from './mfe/load-fragments/load-fragments.component';
import { MfeServiceService } from './mfe/mfe-service.service';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoginComponent } from './login/login.component';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './notifications/notifications/notifications.component';

@NgModule({
  declarations: [
    AppComponent,
    ShellHomeComponent,
    LandingPageComponent,
    LoadFragmentsComponent,
    LoginComponent,
    NotificationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [
    //cấu hình động các route đến các micro
    {
      provide: APP_INITIALIZER,
      useFactory: (mfeService: MfeServiceService) => () =>
        mfeService.init(),
      deps: [MfeServiceService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
