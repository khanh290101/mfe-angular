import { loadRemoteModule } from '@angular-architects/module-federation';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { PluginOptions } from '../../model/mf.model';
@Component({
  selector: 'app-load-fragments',
  template: ` <ng-container #placeHolder></ng-container> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadFragmentsComponent implements OnChanges {
  @ViewChild('placeHolder', { read: ViewContainerRef, static: true })
  viewContainer!: ViewContainerRef;
  @Input() options!: PluginOptions;

  constructor(private cdRef: ChangeDetectorRef) {}

  async ngOnChanges() {
    this.viewContainer.clear();
    const component = await loadRemoteModule(this.options!).then(
      (m) => m[this.options?.componentName]
    );
    this.viewContainer.createComponent(component);
    this.cdRef.detectChanges();
  }
}
