
// import { Injectable } from '@angular/core';
// import { Router } from '@angular/router';
// import { buildRoutes } from './mfe-dynamic.routes';

// @Injectable({
//   providedIn: 'root'
// })
// export class MfeServiceService {
//   constructor(private router: Router) { }
//   init () {
//     return new Promise<void>((resolve, reject) => {
//       const routes = buildRoutes();
//       this.router.resetConfig(routes);
//       resolve();
//     })
//   }
// }


///////==============///////////


import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';
import { buildRoutes } from './mfe-dynamic.routes';

@Injectable({
  providedIn: 'root'
})
export class MfeServiceService {
  constructor(private router: Router, private httpClient: HttpClient) { }

  async init() {
    try {
      const routes = await buildRoutes(this.httpClient);
      this.router.resetConfig(routes);
    } catch (error) {
      console.error('Lỗi khi khởi tạo cấu hình động:', error);
      
    }
  }
}
