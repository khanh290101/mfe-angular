// import { getManifest, loadRemoteModule } from "@angular-architects/module-federation";
// import { Routes } from "@angular/router";
// import { routes } from "../app-routing.module";
// import { CustomManifest } from "../model/mf.model";

// export function buildRoutes(): Routes {
//     const lazyRoutes = Object.entries(getManifest<CustomManifest>())
//         .filter(([key, value]) => {
//             return value.viaRoute === true
//         })
//         .map(([key, value]) => {
//             return {
//                 path: value.routePath,
//                 loadChildren: () => loadRemoteModule({
//                     type: 'manifest',
//                     remoteName: key,
//                     exposedModule: value.exposedModule
//                 }).then(m => m[value.ngModuleName!])
//                 .catch(err => {
//                   console.error(`Lỗi khi tải module từ ${key}:`, err);
//                   // Điều hướng đến một module lỗi
//                   return import('../error/error.module').then(m => m.ErrorModule);
//               })
//             }
//         });
//     const notFound = [
//         {
//             path: '**',
//             redirectTo: ''
//         }]
//     // { path:'**', ...} needs to be the LAST one.
//     return [...routes, ...lazyRoutes, ...notFound]
// }


////===================///////////////

import { HttpClient } from '@angular/common/http';
import { Routes } from '@angular/router';
import { CustomManifest } from '../model/mf.model';
import { loadRemoteModule } from '@angular-architects/module-federation';
import { firstValueFrom } from 'rxjs';
import { routes } from '../app-routing.module';

export async function buildRoutes(httpClient: HttpClient): Promise<Routes> {
  try {
    let manifest: CustomManifest | undefined;

    try {
      manifest = await firstValueFrom(httpClient.get<CustomManifest>('/manifest'));
      
    } catch (apiError) {
      console.error('Lỗi khi tải cấu hình từ API:', apiError);
    }

    // Nếu không thể tải manifest từ server, thử tải manifest cục bộ từ assets
    if (!manifest) {
      try {
        const localManifestResponse = await httpClient.get<CustomManifest>('/assets/mf.manifest.json');
        manifest = await firstValueFrom(localManifestResponse);
        console.log('Sử dụng manifest cục bộ:', manifest);
      } catch (localManifestError) {
        console.error('Lỗi khi tải manifest cục bộ:', localManifestError);
      }
    }

    if (manifest) {
      const lazyRoutes = Object.entries(manifest)
        .filter(([key, value]) => value.viaRoute === true)
        .map(([key, value]) => ({
          path: value.routePath,
          loadChildren: () => loadRemoteModule({
            type: 'manifest',
            remoteName: key,
            exposedModule: value.exposedModule,
          }).then(m => m[value.ngModuleName!])
          .catch(err => {
            console.error(`Lỗi khi tải module từ ${key}:`, err);
            return import('../error/error.module').then(m => m.ErrorModule);
          }),
        }));

      const notFound = [{ path: '**', redirectTo: '' }];
      return [...routes, ...lazyRoutes, ...notFound];
    } else {
      console.error('Không có manifest nào được tải thành công.');

      return [];
    }
  } catch (error) {
    console.error('Lỗi khi xử lý cấu hình:', error);

    return [];
  }
}
