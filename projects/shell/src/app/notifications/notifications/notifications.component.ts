import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-notification',
  template: `<div *ngIf="message" class="notification">{{ message }}</div>`,
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  message: string = '';

  constructor(private notificationService: NotificationService) {}

  ngOnInit(): void {
    this.notificationService.message.subscribe(msg => {
      this.message = msg;
    });
  }
}
