// import('./bootstrap')
// 	.catch(err => console.error(err));
//   import { setManifest } from '@angular-architects/module-federation';
//   import { CustomManifest } from './app/model/mf.model';
//   const mfManifestJson = fetch('/assets/mf.manifest.json');
//   const parseConfig = async (mfManifest: Promise<Response>) => {
//     const manifest: CustomManifest = await (await mfManifest).json();
//     const filterManifest: CustomManifest = {};
//     for (const key of Object.keys(manifest)) {
//       const value = manifest[key];
//       // check more details
//       if (value.isActive === true) {
//         filterManifest[key] = value;
//       }
//     }
//     return filterManifest;
//   };
//   parseConfig(mfManifestJson)
//     .then((data) => setManifest(data))
//     .catch((err) => console.log(err))
//     .then((_) => import('./bootstrap'))
//     .catch((err) => console.log(err));


///////==============///////////

import { setManifest } from '@angular-architects/module-federation';
import { CustomManifest } from './app/model/mf.model';

// Hàm để tải manifest từ server API hoặc manifest cục bộ
const fetchManifest = async () => {
  try {
    const response = await fetch('/manifest');
    if (!response.ok) {
      throw new Error('Lỗi khi tải manifest từ server API');
    }
    const manifest: CustomManifest = await response.json();
    setManifest(manifest); // Sử dụng manifest từ server API
  } catch (error) {
    console.error('Lỗi khi tải manifest từ server API:', error);

    // Nếu có lỗi khi tải manifest từ server API, tải manifest cục bộ
    try {
      const localManifestResponse = await fetch('/assets/mf.manifest.json');
      if (!localManifestResponse.ok) {
        throw new Error('Lỗi khi tải manifest cục bộ');
      }
      const localManifest: CustomManifest = await localManifestResponse.json();
      setManifest(localManifest); // Sử dụng manifest cục bộ
    } catch (localManifestError) {
      console.error('Lỗi khi tải manifest cục bộ:', localManifestError);
    }
  } finally {
    import('./bootstrap')
      .catch((err) => console.log(err));
  }
};

fetchManifest();
