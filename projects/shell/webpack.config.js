const {
  shareAll,
  withModuleFederationPlugin,
} = require('@angular-architects/module-federation/webpack');

module.exports = withModuleFederationPlugin({
  //đã được cấu hình load động ở mfe/mfe-dynamic.routers
  // remotes: {
  //   "screen1": "http://localhost:5001/remoteEntry.js",
  //   "screen2": "http://localhost:5002/remoteEntry.js",
  //   "screen3": "http://localhost:5003/remoteEntry.js",
  // },
  shared: {
    ...shareAll({
      singleton: true,
      strictVersion: true,
      requiredVersion: 'auto',
    }),
  },
});
