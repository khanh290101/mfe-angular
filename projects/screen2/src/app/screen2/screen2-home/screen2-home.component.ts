import { Component } from '@angular/core';
import { capitalizeFirstLetter, formatDate } from 'utils-lib';

@Component({
  selector: 'app-screen2-home',
  templateUrl: './screen2-home.component.html',
  styleUrls: ['./screen2-home.component.scss']
})
export class Screen2HomeComponent {
  textInput: string = '';
  formattedText: string = '';
  selectedDate: Date = new Date();
  formattedDate: string = '';

  onSubmit(): void {
    this.formattedText = capitalizeFirstLetter(this.textInput);
  }

  onDateChange(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.formattedDate = formatDate(new Date(target.value), "dd-MM-YYYY");
  }
}
