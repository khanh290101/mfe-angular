import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../screen3-service/products.service';

@Component({
  selector: 'app-screen3-home',
  templateUrl: './screen3-home.component.html',
  styleUrls: ['./screen3-home.component.scss']
})
export class Screen3HomeComponent implements OnInit {
  products: any[] = [];
  p: number = 1;

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.loadProducts();
  }

  loadProducts(): void {
    this.productsService.getProducts().subscribe(data => {
      this.products = data;
    });
  }
}
