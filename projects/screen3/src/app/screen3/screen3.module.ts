import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Screen3HomeComponent } from './screen3-home/screen3-home.component';
import { Screen3RoutingModule } from './screen3-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    Screen3HomeComponent,
  ],
  imports: [
    CommonModule,
    Screen3RoutingModule,
    HttpClientModule,
    NgxPaginationModule
  ]
})
export class Screen3Module { }
