import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthLibService } from 'auth-lib';

@Component({
  selector: 'app-screen1-home',
  templateUrl: './screen1-home.component.html',
  styleUrls: ['./screen1-home.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class Screen1HomeComponent implements OnInit {
  userName!: string;
  token!: string;

  constructor(private authLibService: AuthLibService) {}

  ngOnInit() {
    this.userName = this.authLibService.user;
    this.token = this.authLibService.authToken;
  }
}
