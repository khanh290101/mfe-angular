export function formatDate(date: Date, format: string): string {
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const year = date.getFullYear();

  if (format === "dd-MM-YYYY") {
      return `${day}-${month}-${year}`;
  }
  return date.toISOString();
}
